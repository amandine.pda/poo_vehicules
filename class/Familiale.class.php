<?php

class Familiale extends Vehicule{

    use Revisable;

    private int $passagers_max;
    const FREQUENCE = 30000;  

    public function __construct(int $id, string $marque, string $modele, int $km, int $passagers_max)
    {
        
        parent::__construct($id, $marque, $modele, $km);
        $this->setPassagers_max($passagers_max);
    }

    /**
     * Get the value of passagers_max
     */ 
    public function getPassagers_max(): int
    {
        return $this->passagers_max;
    }

    /**
     * Set the value of passagers_max
     *
     * @return  self
     */ 
    private function setPassagers_max($passagers_max): self
    {
        $this->passagers_max = $passagers_max;

        return $this;
    }

    /**
     * The function __toString() is a magic method that returns a string representation of the object
     * 
     * @return string The parent class's __toString() method is being called, and the result is
     * concatenated with the result of the getPassagers_max() method.
     */
    private function __toString(): string
    {
       return parent::__toString() . "Nbr de passagers max : {$this->getPassagers_max()}\n"; 
    }

}