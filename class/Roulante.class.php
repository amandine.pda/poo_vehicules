<?php

interface Roulante {
    public function rouler(int $km);
}