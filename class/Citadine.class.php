<?php

class Citadine extends Vehicule implements Revisable{

    use Revisable;

    private float $autonomie;
    const FREQUENCE = 15000;
    
    public function __construct(int $id, string $marque, string $modele, int $km, float $autonomie)
    {
        parent::__construct($id,$marque,$modele,$km);
        $this->setAutonomie($autonomie);
    }

    /**
     * Get the value of autonomie
     */ 
    public function getAutonomie(): float
    {
        return $this->autonomie;
    }

    /**
     * Set the value of autonomie
     *
     * @return  self
     */ 
    private function setAutonomie($autonomie): self
    {
        $this->autonomie = $autonomie;

        return $this;
    }

    /**
     * The function `__toString()` is a magic method that is called when an object is used in a string
     * context
     * 
     * @return string The parent class's __toString() method is being called, and the result is being
     * concatenated with the string "Autonomie : {->getAutonomie()}\n".
     */
    private function __toString(): string
    {
        return parent::__toString() . "Autonomie : {$this->getAutonomie()}\n";
    }


    

}