<?php

class Utilitaire extends Vehicule {

    use Revisable;

    private int $charge_utile_en_kg;
    const FREQUENCE = 30000;
      

    public function __construct(int $id, string $marque, string $modele, int $km, int $charge_utile_en_kg)
    {
        parent::__construct($id, $marque, $modele, $km);
        $this->setCharge_utile_en_kg($charge_utile_en_kg);
    }

    /**
     * Get the value of charge_utile_en_kg
     */ 
    public function getCharge_utile_en_kg(): int
    {
        return $this->charge_utile_en_kg;
    }

    /**
     * Set the value of charge_utile_en_kg
     *
     * @return  self
     */ 
    private function setCharge_utile_en_kg($charge_utile_en_kg): self
    {
        $this->charge_utile_en_kg = $charge_utile_en_kg;

        return $this;
    }

    /**
     * The function `__toString()` is a magic method that is called when an object is used in a string
     * context
     * 
     * @return string The parent class's __toString() method is being called, and then the child class's
     * __toString() method is being called.
     */
    private function __toString(): string
    {
        return parent::__toString() . "Charge utile en kg : {$this->getCharge_utile_en_kg()}\n";
    }

}