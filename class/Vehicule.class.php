<?php

abstract class Vehicule implements Roulante{


    const KM_MAX = 120000;
    protected int $id;
    protected string $marque;
    protected string $modele;
    protected int $km;
    

    public function __construct(int $id, string $marque, string $modele, int $km)
    {
        $this->setId($id)->setMarque($marque)->setModele($modele)->setKm($km);
    }

    /**
     * Get the value of id
     */ 
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    private function setId($id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of marque
     */ 
    public function getMarque(): string
    {
        return $this->marque;
    }

    /**
     * Set the value of marque
     *
     * @return  self
     */ 
    private function setMarque($marque): self
    {
        $this->marque = $marque;

        return $this;
    }

    /**
     * Get the value of modele
     */ 
    public function getModele(): string
    {
        return $this->modele;
    }

    /**
     * Set the value of modele
     *
     * @return  self
     */ 
    private function setModele($modele): self
    {
        $this->modele = $modele;

        return $this;
    }

    /**
     * Get the value of km
     */ 
    public function getKm(): int
    {
        return $this->km;
    }

    /**
     * Set the value of km
     *
     * @return  self
     */ 
    private function setKm($km): self
    {
        $this->km = $km;

        return $this;
    }

    /**
     * The __toString() method is a magic method that is called when an object is used in a string context.
     */
    protected function __toString(): string
    {
        return "Marque du véhicule : {$this->getMarque()}\n
                Modèle du véhicule : {$this->getModele()}\n
                Kilométrage : {$this->getKm()}\n";
    }

    /**
     * The function `rouler` takes a parameter `km` and adds it to the current value of anterior km stored
     * in the object. If the new value of anterior km is greater than KM_MAX = 120 000, the function prints a message
     * and does not update the value of anterior km
     * 
     * @param km the number of kilometers the car have to drive
     */
    public function rouler ($km)
    {
        $ant_km = $this->getKm();
        if($ant_km<=self::KM_MAX)
        {
            echo "Vroom vroom, ça roule ma poule";
            $this->setKm($ant_km + $km);
        }
        else{
            echo "Vous avez parcouru vos 120 000 km, repose soldat !";
        }
    }

}