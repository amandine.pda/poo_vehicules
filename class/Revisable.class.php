<?php

trait Revisable{

    public function reviser(){
        $frequence = $this->self::FREQUENCE;

        if($this->getKM()>=$frequence )
        {
            echo "Il est temps de passer à la révision";
        }
        else
        {
            $km_restant =  $frequence - $this->getKM();
            echo "Roule ma poule, il te reste ". $km_restant . " avant la révision";
        }
    }

}